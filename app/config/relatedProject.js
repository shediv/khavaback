var relatedProject ={

	"magazine" : {
	  'urlSlug' : 1,
    'name': 1,
    'thumbnail' : 1,
    'attributes' : 1,
    'categoryId' : 1,
    '_id' : 1,
    'logo': 1,
    'print.mediaOptions.fullPage.cardRate' : 1
	},

	"newspaper" : {
		'_id' : 1,
    'urlSlug' : 1,
    'name'       : 1,
    'editionName' : 1,
    'circulation' : 1,
    'areaCovered' : 1,
    'language' : 1,
    'urlSlug' : 1,
    'mediaOptions.regularOptions.anyPage.<800SqCms.cardRate' : 1,
    'logo' : 1
	},

	"radio": {
		'_id' : 1,
    'radioFrequency' : 1,
    'station' : 1,
    'geography' : 1,
    'language' : 1,
    'mediaOptions.regularOptions' : 1,        
    'logo' : 1
	}
} 

module.exports = relatedProject;